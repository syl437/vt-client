'use strict';
angular.module('main', [
    'ionic',
    'ngCordova',
    'ui.router',
    'restangular',
    'ngStorage',
    'google.places',
    'ngFileUpload'

])
    .config(function ($compileProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider) {

        // Needed to be able to display images in LiveReload mode on devices
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);

        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self'
        ]);

        // ROUTING with ui.router
        $urlRouterProvider.otherwise('/main/login');
        $stateProvider

            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'main/templates/menu.html',
                controller: 'MenuCtrl as menu'
            })

            .state('main.login', {
                url: '/login',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('main.register', {
                url: '/register',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('main.register_company', {
                url: '/register_company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register_company.html',
                        controller: 'RegisterCompanyCtrl'
                    }
                },
                resolve: {
                    company_categories: function (Restangular) { return Restangular.all('company_categories').getList();},
                    regions: function (Restangular) { return Restangular.all('regions').getList();}
                }
            })

            .state('main.register_company_additional', {
                url: '/register_company_additional',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register_company_additional.html',
                        controller: 'RegisterCompanyAdditionalCtrl'
                    }
                },
                resolve: {company: function (Restangular, $localStorage) {return Restangular.one('companies', $localStorage.company_id).get();}}
            })

            .state('main.home', {
                url: '/home',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/home.html',
                        controller: 'HomeCtrl'
                    }
                },
                resolve: {
                    company_categories: function (Restangular) { return Restangular.all('company_categories').getList();},
                    regions: function (Restangular) { return Restangular.all('regions').getList();}
                }
            })

            .state('main.companies', {
                url: '/companies/:category/:region',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/companies.html',
                        controller: 'CompaniesCtrl'
                    }
                },
                resolve: {companies: function (Restangular, $stateParams) { return Restangular.all('companies').getList({region_id: $stateParams.region, company_category_id: $stateParams.category});}}
            })

            .state('main.profile', {
                url: '/profile',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/profile.html',
                        controller: 'ProfileCtrl'
                    }
                },
                resolve: {user: function (Restangular, $localStorage) { return Restangular.one('users', $localStorage.id).get();}}
            })

            .state('main.login_company', {
                url: '/login_company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/login_company.html',
                        controller: 'LoginCompanyCtrl'
                    }
                }
            })

            .state('main.company', {
                url: '/company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/company.html',
                        controller: 'CompanyCtrl'
                    }
                },
                resolve: {company: function (Restangular, $localStorage) { return Restangular.one('companies', $localStorage.company_id).get();}}

            })
        ;
    })

    .run(function($state, $localStorage, $log, $ionicLoading, Restangular, Config, $ionicPopup){

        // See app/main/constants/env-dev.json and app/main/constants/env-prod.json
        Restangular.setBaseUrl(Config.ENV.SERVER_URL);
        // Restangular.setBaseUrl('http://vt.dev/api/v1');

        // Attach phone number as token for each API request
        Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers) {

            return {
                headers: _.merge({Authorization: 'Bearer ' + $localStorage.token}, headers)
            };

        });

        // handle request

        Restangular.addRequestInterceptor(function(element, operation, what, url){

            $ionicLoading.show({template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'});

            return element;

        });

        // Handle response

        Restangular.addResponseInterceptor(function (data, operation, what, url) {

            if (data.data) {
                $log.log(url, Restangular.copy(data.data).plain());
            }

            var extractedData;

            if (operation === 'getList')
                extractedData = data.data;
            else
                extractedData = data.data;

            $ionicLoading.hide();

            return extractedData;

        });

        Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {

            if (response){

                $ionicLoading.hide();

                switch (response.status){

                    case 401:
                        $ionicPopup.alert({title: "Error 401"});
                        $state.go('main.login');
                        return false;
                        break;

                    case 404:
                        $ionicPopup.alert({title: 'Error 404'});
                        return false;
                        break;

                    case 422:
                        $ionicPopup.alert({title: 'Error 422'});
                        return false;
                        break;

                    default:
                        $ionicPopup.alert({title: 'Error!'});
                        return false;
                        break;

                }

            }

            return true;
        });

        Restangular.addElementTransformer('companies', false, function (file) {

            file.addRestangularMethod('reupload', 'post', undefined, {'_method': 'PATCH'}, {'Content-Type': undefined});

            return file;
        });

    })
;


// сделать валидацию телефона при апдейте профиля по стране
// разобраться с адресом
// exception 401 при несовпадении паролей при изменении пароля - сделать другой
// добавить в исключения gitignore папку media
// сделать отдельный реквест для создания компаний
// переделать логин для компаний на сервере
'use strict';
angular.module('main')
    .service('Utils', function ($ionicPopup) {

        // Public

        this.isAnyFieldEmpty = isAnyFieldEmpty;

        // Private

        function isAnyFieldEmpty(x){

            var keys = Object.getOwnPropertyNames(x);

            for (var key in keys){

                if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null){

                    $ionicPopup.alert({title: "נא למלא את כל השדות"});

                    return true;

                }

            }

            return false;

        }

    });

'use strict';
angular.module('main')
    .service('Main', function ($log, $timeout) {


    });

'use strict';
angular.module('main')
    .service('FileService', function ($log, $q) {

        // Public

        this.convertFileUriToFile = convertFileUriToFile;
        this.convertFileUrisToFiles = convertFileUrisToFiles;
        this.convertFileUriToInternalURL = convertFileUriToInternalURL;

        // Private

        // Converts File URI to proper Blob that will be sent to
        // server within FormData bag. We need it just because
        // iOS doesn't work good with FormData and this is the only
        // way to implement it
        function convertFileUriToFile(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                // .file method is accepting a callback that receives file (which is instance of
                // File object that will be converted to Blob)
                fileEntry.file(function (file) {

                    // initialize FileReader
                    var reader = new FileReader();

                    // attach callback that will run upon FileReader finishes reading file
                    reader.onloadend = function (evt) {

                        deferred.resolve(new Blob([evt.target.result]), fileUri);

                    };

                    reader.onerror = function () {

                        deferred.reject();

                    };

                    // read file as array buffer
                    reader.readAsArrayBuffer(file);

                });
            }

            return deferred.promise;

        }

        function convertFileUrisToFiles(fileUris) {

            return $q.all(fileUris.map(function (file) {

                return convertFileUriToFile(file.url || file);

            }));
        }

        // Converts File URI to proper Internal URL that will be used
        // to display local device images while being in LiveReload mode
        function convertFileUriToInternalURL(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved, onFileResolveError);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                deferred.resolve(fileEntry.toInternalURL());
            }

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolveError() {

                deferred.reject();
            }

            return deferred.promise;
        }

    });

'use strict';
angular.module('main')
    .directive('chooseVideo', function ($cordovaCamera, $ionicPopup) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {

            },
            scope: {video: "="},
            controller: function ($scope, $element) {

                $element.on('click', function () {

                    navigator.device.capture.captureVideo(captureSuccess, captureError, {duration: 30});

                });

                // Functions

                function captureSuccess(mediaFiles) {

                    console.log("captureSuccess", mediaFiles);

                    mediaFiles[0].getFormatData(function (data) {

                        console.log("data", data);

                        var file = {};

                        file.fullPath = mediaFiles[0].fullPath;
                        file.localURL = mediaFiles[0].localURL;
                        file.duration = data.duration + ' sec';
                        file.size = parseInt(mediaFiles[0].size / 1000000) + ' MB';
                        file.type = mediaFiles[0].type;
                        file.name = mediaFiles[0].name;

                        $scope.video = file;


                    }, function (error) {

                        console.log('A Format Data Error occurred during getFormatData: ' + error.code);

                    });

                }

                function captureError (error) {
                    console.log(error);
                    alert('Returncode: ' + JSON.stringify(error.code));
                }

            }
        };
    });

'use strict';
angular.module('main')
    .directive('choosePicture', function ($cordovaCamera, $ionicPopup, $log, FileService) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {

            },
            scope: {pictures: "="},
            controller: function ($scope, $element) {

                $element.on('click', function () {

                    var myPopup = $ionicPopup.show({
                        title: 'בחר/י מקור התמונה',
                        scope: $scope,
                        cssClass: 'custom-popup',
                        buttons: [
                            {text: '<i class="icon ion-ios-camera"></i>', type: 'button-positive', onTap: function () {addImage(1);}},
                            {text: '<i class="icon ion-images"></i>', type: 'button-calm', onTap: function () {addImage(0);}},
                            {text: 'ביטול', type: 'button-assertive', onTap: function () {}}
                        ]
                    });

                });

                // Methods

                $scope.addImage = addImage;

                // Functions

                function addImage(index) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: true,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: 800,
                        targetHeight: 600,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    };

                    // Get fileUri from gallery (see Camera.DestinationType.FILE_URI in options)
                    $cordovaCamera.getPicture(options).then(function (fileUri) {

                        FileService.convertFileUriToInternalURL(fileUri).then(function(uri) {

                            if (angular.isArray($scope.pictures))
                                $scope.pictures.push({url: uri});
                            else
                                $scope.pictures = uri;

                        });

                    });
                }

            }
        };
    });

'use strict';
angular.module('main')
    .controller('RegisterCtrl', function ($state, $localStorage, Utils, $log, Restangular, $scope) {

        $scope.user = Restangular.restangularizeElement('', {email: '', password: '', name: '', phone: '', address: ''}, 'users');

        $scope.register = register;

        function register() {

            if (!Utils.isAnyFieldEmpty($scope.user.plain())){

                var addressKeys = Object.keys($scope.user.address.address_components);
                var countryComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 1]];
                var cityComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 3]];

                $scope.user.city = $scope.user.address.address_components[cityComponent].long_name;
                $scope.user.country = $scope.user.address.address_components[countryComponent].short_name;

                $scope.user.save().then(function (data) {

                    $localStorage.token = data.token;
                    $localStorage.id = data.id;
                    $state.go('app.home');

                })
            }
        }

    });

'use strict';
angular.module('main')
    .controller('RegisterCompanyCtrl', function ($state, $localStorage, Utils, $ionicPopup, Restangular, $scope, $log, company_categories, regions) {

        $scope.company_categories = company_categories;
        $scope.regions = regions;
        $scope.company = Restangular.restangularizeElement('', {email: '', password: '', title: '', phone: '', address: '', description: '', company_category_id: '0', company_subcategory_id: '0', region_id: '0'}, 'companies');

        $scope.register = register;

        function register() {

            var addressKeys = Object.keys($scope.company.address.address_components);
            var lastComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 1]];
            $scope.company.lat = $scope.company.address.geometry.location.lat();
            $scope.company.lng = $scope.company.address.geometry.location.lng();
            $scope.company.country = $scope.company.address.address_components[lastComponent].short_name;
            $scope.company.address = $scope.company.address.formatted_address;
            $scope.user_id = $localStorage.id;

            $log.log($scope.company.plain());

            if ($scope.company.company_category_id === '0' || $scope.company.company_subcategory_id === '0' || $scope.company.region_id === '0'){

                $ionicPopup.alert({title: 'Please select all!'});
                return;

            }

            if (!Utils.isAnyFieldEmpty($scope.company.plain())){

                $scope.company.save().then(function (data) {

                    $localStorage.company_id = data.id;
                    $state.go('main.register_company_additional');

                })
            }

        }

    });

'use strict';
angular.module('main')
    .controller('RegisterCompanyAdditionalCtrl', function (FileService, Restangular, $ionicPopup, $localStorage, $ionicLoading, $scope, $log, company) {

        $scope.company = company;
        $scope.logo = {file: ''};
        $scope.video = {file: ''};

        // $scope.uploadVideo = uploadVideo;
        $scope.selectFile = selectFile;
        $scope.uploadFiles = uploadFiles;

        $scope.$watch('video', function(){

            if ($scope.video.file.fullPath){

                console.log($scope.video);

                document.getElementById('home_video').innerHTML = $scope.video.file.size + ' ' + $scope.video.file.type;

                var vid = document.createElement('video');
                vid.id = "theVideo";
                vid.width = "240";
                vid.height = "160";
                vid.controls = "controls";
                var source_vid = document.createElement('source');
                source_vid.id = "theSource";
                source_vid.src = $scope.video.file.fullPath;
                vid.appendChild(source_vid);
                document.getElementById('home_video_container').innerHTML = '';
                document.getElementById('home_video_container').appendChild(vid);

            }

        }, true);

        // function uploadVideo() {
        //
        //     if (($scope.video.file.size / 1000000) > 100){
        //
        //         $ionicPopup.alert({title: "Too big!"});
        //         return;
        //
        //     }
        //
        //     $ionicLoading.show();
        //
        //     var options = new FileUploadOptions();
        //     options.fileKey = "main_video";
        //     options.mimeType = "mp4";
        //     options.fileName = $scope.video.file.fullPath.substr($scope.video.file.fullPath.lastIndexOf('/') + 1);
        //     options.params = {_method : "PUT"};
        //     options.headers = {Authorization: 'Bearer ' + $localStorage.token};
        //
        //     var ft = new FileTransfer();
        //
        //     ft.upload($scope.video.file.fullPath, encodeURI(Config.ENV.SERVER_URL + "/companies/" + $localStorage.company_id), onUploadSuccess, onUploadFail, options);
        //
        // }

        // function onUploadSuccess(data) {
        //
        //     console.log(data);
        //     $ionicPopup.alert({title: "Successfully uploaded!"});
        //     $ionicLoading.hide();
        //
        // }
        //
        // function onUploadFail(error) {
        //
        //     console.log('error', error);
        //     $ionicPopup.alert({title: "Something's wrong"});
        //     $ionicLoading.hide();
        //
        // }

        function selectFile(file) {

            if ((file.size / 1000000) > 100){

                $ionicPopup.alert({title: "Too big!"});
                return;

            }

            $scope.video.file = file;
            console.log(file);



            // document.getElementById('home_video').innerHTML = $scope.video.file.size + ' ' + $scope.video.file.type;
            //
            // var vid = document.createElement('video');
            // vid.id = "theVideo";
            // vid.width = "240";
            // vid.height = "160";
            // vid.controls = "controls";
            // var source_vid = document.createElement('source');
            // source_vid.id = "theSource";
            // source_vid.src = $scope.video.file.fullPath;
            // vid.appendChild(source_vid);
            // document.getElementById('home_video_container').innerHTML = '';
            // document.getElementById('home_video_container').appendChild(vid);

        }

        function uploadFiles () {

            console.log($scope.video.file);
            console.log($scope.logo.file);

            if ($scope.video.file !== ''){

                var formData = new FormData();

                formData.append('main_video', $scope.video.file);

                Restangular.one('companies', $localStorage.company_id).withHttpConfig({transformRequest: angular.identity, timeout: 0}).reupload(formData)

                    .then(function (response) {

                        $scope.video = {file: ''};
                        $ionicPopup.alert({title: "Video is successfully uploaded!"});

                    });

            }

            if ($scope.logo.file !== ''){

                FileService.convertFileUriToFile($scope.logo.file).then(function(file){

                    var formData2 = new FormData();

                    formData2.append('logo', file);

                    Restangular.one('companies', $localStorage.company_id).withHttpConfig({transformRequest: angular.identity, timeout: 0}).reupload(formData2)

                        .then(function (response) {

                            $scope.logo = {file: ''};
                            $ionicPopup.alert({title: "Logo is successfully uploaded!"});

                        });

                })

            }

        }

    });

'use strict';
angular.module('main')
    .controller('ProfileCtrl', function ($localStorage, Restangular, $ionicPopup, Utils, $scope, $log, user) {

        $scope.user = user;
        $scope.credentials = {old_password: '', new_password: '', password_confirmation: ''};

        $scope.update = update;
        $scope.changePassword = changePassword;

        function update() {

            if (!Utils.isAnyFieldEmpty($scope.user.plain())){

                $scope.user.put().then(function (data) {$scope.user = data;})

            }

        }

        function changePassword() {

            if ($scope.credentials.new_password !== $scope.credentials.password_confirmation){

                $ionicPopup.alert({title: "Passwords don't match!"});
                return;

            }

            if (!Utils.isAnyFieldEmpty($scope.credentials)){

                Restangular.one('users', $localStorage.id).all('password').post($scope.credentials).then(function () {

                    $scope.credentials = {old_password: '', new_password: '', password_confirmation: ''};
                    $ionicPopup.alert({title: "Successfully updated!"});

                })

            }

        }

    });

'use strict';
angular.module('main')
    .controller('MenuCtrl', function ($log) {


    });

'use strict';
angular.module('main')
    .controller('LoginCtrl', function ($state, $localStorage, Restangular, $ionicPopup, $rootScope, $scope) {

        // Data

        $scope.login = {email: 'test@test.com', password: '123456'};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.login.email === "" || $scope.login.password === "") {

                $ionicPopup.alert({title: 'Please fill up all fields!'});
                return;

            }

            Restangular.all('tokens').post($scope.login).then(authenticationSuccess);
        }

        function authenticationSuccess(data) {

            $localStorage.token = data.token;
            $localStorage.id = data.id;
            $state.go('main.home');

        }

    });

'use strict';
angular.module('main')
    .controller('LoginCompanyCtrl', function ($state, $localStorage, Restangular, $ionicPopup, $scope, $log) {

        // Data

        $scope.company = {email: 'hotel@test.com', password: '123456'};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.company.email === "" || $scope.company.password === "") {

                $ionicPopup.alert({title: 'Please fill up all fields!'});
                return;

            }

            Restangular.all('companies').all('login').post($scope.company).then(authenticationSuccess);
        }

        function authenticationSuccess(data) {

            $localStorage.company_id = data.id;
            $state.go('main.company');

        }


    });

'use strict';
angular.module('main')
    .controller('HomeCtrl', function ($ionicLoading, $localStorage, Restangular, FileService, $ionicPopup, $timeout, $state, $log, $scope, company_categories, regions) {

       $scope.company_categories = company_categories;
       $scope.regions = regions;
       $scope.data = {region: '0', category: '0'};

    });

'use strict';
angular.module('main')
    .controller('CompanyCtrl', function ($scope, company, $log) {

        $scope.company = company;


    });

'use strict';
angular.module('main')
    .controller('CompaniesCtrl', function ($stateParams, $scope, $log) {



    });

'use strict';
angular.module('main')
    .constant('Config', {

        // gulp environment: injects environment vars
        ENV: {
            /*inject-env*/
            'SERVER_URL': 'http://vt.applicazza.net/api/v1'
            /*endinject*/
        },

        // gulp build-vars: injects build vars
        BUILD: {
            /*inject-build*/
            /*endinject*/
        }

    });
'use strict';
angular.module('vtClient', [
  // load your modules here
  'main', // starting with the main module
]);
