'use strict';

describe('module: main, controller: CompaniesCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var CompaniesCtrl;
  beforeEach(inject(function ($controller) {
    CompaniesCtrl = $controller('CompaniesCtrl');
  }));

  it('should do something', function () {
    expect(!!CompaniesCtrl).toBe(true);
  });

});
