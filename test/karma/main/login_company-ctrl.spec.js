'use strict';

describe('module: main, controller: LoginCompanyCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var LoginCompanyCtrl;
  beforeEach(inject(function ($controller) {
    LoginCompanyCtrl = $controller('LoginCompanyCtrl');
  }));

  it('should do something', function () {
    expect(!!LoginCompanyCtrl).toBe(true);
  });

});
