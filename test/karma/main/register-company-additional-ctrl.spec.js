'use strict';

describe('module: main, controller: RegisterCompanyAdditionalCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var RegisterCompanyAdditionalCtrl;
  beforeEach(inject(function ($controller) {
      RegisterCompanyAdditionalCtrl = $controller('RegisterCompanyAdditionalCtrl');
  }));

  it('should do something', function () {
    expect(!!RegisterCompanyAdditionalCtrl).toBe(true);
  });

});
