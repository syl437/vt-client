'use strict';

describe('module: main, controller: RegisterCompanyCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var RegisterCompanyCtrl;
  beforeEach(inject(function ($controller) {
    RegisterCompanyCtrl = $controller('RegisterCompanyCtrl');
  }));

  it('should do something', function () {
    expect(!!RegisterCompanyCtrl).toBe(true);
  });

});
