'use strict';
angular.module('main')
    .controller('LoginCtrl', function ($state, $localStorage, Restangular, $ionicPopup, $rootScope, $scope) {

        // Data

        $scope.login = {email: 'test@test.com', password: '123456'};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.login.email === "" || $scope.login.password === "") {

                $ionicPopup.alert({title: 'Please fill up all fields!'});
                return;

            }

            Restangular.all('tokens').post($scope.login).then(authenticationSuccess);
        }

        function authenticationSuccess(data) {

            $localStorage.token = data.token;
            $localStorage.id = data.id;
            $state.go('main.home');

        }

    });
