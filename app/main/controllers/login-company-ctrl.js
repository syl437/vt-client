'use strict';
angular.module('main')
    .controller('LoginCompanyCtrl', function ($state, $localStorage, Restangular, $ionicPopup, $scope, $log) {

        // Data

        $scope.company = {email: 'hotel@test.com', password: '123456'};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.company.email === "" || $scope.company.password === "") {

                $ionicPopup.alert({title: 'Please fill up all fields!'});
                return;

            }

            Restangular.all('companies').all('login').post($scope.company).then(authenticationSuccess);
        }

        function authenticationSuccess(data) {

            $localStorage.company_id = data.id;
            $state.go('main.company');

        }


    });
