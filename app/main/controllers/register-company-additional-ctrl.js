'use strict';
angular.module('main')
    .controller('RegisterCompanyAdditionalCtrl', function (FileService, Restangular, $ionicPopup, $localStorage, $ionicLoading, $scope, $log, company) {

        $scope.company = company;
        $scope.logo = {file: ''};
        $scope.video = {file: ''};

        // $scope.uploadVideo = uploadVideo;
        $scope.selectFile = selectFile;
        $scope.uploadFiles = uploadFiles;

        $scope.$watch('video', function(){

            if ($scope.video.file.fullPath){

                console.log($scope.video);

                document.getElementById('home_video').innerHTML = $scope.video.file.size + ' ' + $scope.video.file.type;

                var vid = document.createElement('video');
                vid.id = "theVideo";
                vid.width = "240";
                vid.height = "160";
                vid.controls = "controls";
                var source_vid = document.createElement('source');
                source_vid.id = "theSource";
                source_vid.src = $scope.video.file.fullPath;
                vid.appendChild(source_vid);
                document.getElementById('home_video_container').innerHTML = '';
                document.getElementById('home_video_container').appendChild(vid);

            }

        }, true);

        // function uploadVideo() {
        //
        //     if (($scope.video.file.size / 1000000) > 100){
        //
        //         $ionicPopup.alert({title: "Too big!"});
        //         return;
        //
        //     }
        //
        //     $ionicLoading.show();
        //
        //     var options = new FileUploadOptions();
        //     options.fileKey = "main_video";
        //     options.mimeType = "mp4";
        //     options.fileName = $scope.video.file.fullPath.substr($scope.video.file.fullPath.lastIndexOf('/') + 1);
        //     options.params = {_method : "PUT"};
        //     options.headers = {Authorization: 'Bearer ' + $localStorage.token};
        //
        //     var ft = new FileTransfer();
        //
        //     ft.upload($scope.video.file.fullPath, encodeURI(Config.ENV.SERVER_URL + "/companies/" + $localStorage.company_id), onUploadSuccess, onUploadFail, options);
        //
        // }

        // function onUploadSuccess(data) {
        //
        //     console.log(data);
        //     $ionicPopup.alert({title: "Successfully uploaded!"});
        //     $ionicLoading.hide();
        //
        // }
        //
        // function onUploadFail(error) {
        //
        //     console.log('error', error);
        //     $ionicPopup.alert({title: "Something's wrong"});
        //     $ionicLoading.hide();
        //
        // }

        function selectFile(file) {

            if ((file.size / 1000000) > 100){

                $ionicPopup.alert({title: "Too big!"});
                return;

            }

            $scope.video.file = file;
            console.log(file);



            // document.getElementById('home_video').innerHTML = $scope.video.file.size + ' ' + $scope.video.file.type;
            //
            // var vid = document.createElement('video');
            // vid.id = "theVideo";
            // vid.width = "240";
            // vid.height = "160";
            // vid.controls = "controls";
            // var source_vid = document.createElement('source');
            // source_vid.id = "theSource";
            // source_vid.src = $scope.video.file.fullPath;
            // vid.appendChild(source_vid);
            // document.getElementById('home_video_container').innerHTML = '';
            // document.getElementById('home_video_container').appendChild(vid);

        }

        function uploadFiles () {

            console.log($scope.video.file);
            console.log($scope.logo.file);

            if ($scope.video.file !== ''){

                var formData = new FormData();

                formData.append('main_video', $scope.video.file);

                Restangular.one('companies', $localStorage.company_id).withHttpConfig({transformRequest: angular.identity, timeout: 0}).reupload(formData)

                    .then(function (response) {

                        $scope.video = {file: ''};
                        $ionicPopup.alert({title: "Video is successfully uploaded!"});

                    });

            }

            if ($scope.logo.file !== ''){

                FileService.convertFileUriToFile($scope.logo.file).then(function(file){

                    var formData2 = new FormData();

                    formData2.append('logo', file);

                    Restangular.one('companies', $localStorage.company_id).withHttpConfig({transformRequest: angular.identity, timeout: 0}).reupload(formData2)

                        .then(function (response) {

                            $scope.logo = {file: ''};
                            $ionicPopup.alert({title: "Logo is successfully uploaded!"});

                        });

                })

            }

        }

    });
