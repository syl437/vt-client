'use strict';
angular.module('main')
    .controller('HomeCtrl', function ($ionicLoading, $localStorage, Restangular, FileService, $ionicPopup, $timeout, $state, $log, $scope, company_categories, regions) {

       $scope.company_categories = company_categories;
       $scope.regions = regions;
       $scope.data = {region: '0', category: '0'};

    });
