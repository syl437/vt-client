'use strict';
angular.module('main')
    .controller('RegisterCtrl', function ($state, $localStorage, Utils, $log, Restangular, $scope) {

        $scope.user = Restangular.restangularizeElement('', {email: '', password: '', name: '', phone: '', address: ''}, 'users');

        $scope.register = register;

        function register() {

            if (!Utils.isAnyFieldEmpty($scope.user.plain())){

                var addressKeys = Object.keys($scope.user.address.address_components);
                var countryComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 1]];
                var cityComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 3]];

                $scope.user.city = $scope.user.address.address_components[cityComponent].long_name;
                $scope.user.country = $scope.user.address.address_components[countryComponent].short_name;

                $scope.user.save().then(function (data) {

                    $localStorage.token = data.token;
                    $localStorage.id = data.id;
                    $state.go('app.home');

                })
            }
        }

    });
