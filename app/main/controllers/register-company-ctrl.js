'use strict';
angular.module('main')
    .controller('RegisterCompanyCtrl', function ($state, $localStorage, Utils, $ionicPopup, Restangular, $scope, $log, company_categories, regions) {

        $scope.company_categories = company_categories;
        $scope.regions = regions;
        $scope.company = Restangular.restangularizeElement('', {email: '', password: '', title: '', phone: '', address: '', description: '', company_category_id: '0', company_subcategory_id: '0', region_id: '0'}, 'companies');

        $scope.register = register;

        function register() {

            var addressKeys = Object.keys($scope.company.address.address_components);
            var lastComponent = addressKeys[Object.keys(addressKeys)[Object.keys(addressKeys).length - 1]];
            $scope.company.lat = $scope.company.address.geometry.location.lat();
            $scope.company.lng = $scope.company.address.geometry.location.lng();
            $scope.company.country = $scope.company.address.address_components[lastComponent].short_name;
            $scope.company.address = $scope.company.address.formatted_address;
            $scope.user_id = $localStorage.id;

            $log.log($scope.company.plain());

            if ($scope.company.company_category_id === '0' || $scope.company.company_subcategory_id === '0' || $scope.company.region_id === '0'){

                $ionicPopup.alert({title: 'Please select all!'});
                return;

            }

            if (!Utils.isAnyFieldEmpty($scope.company.plain())){

                $scope.company.save().then(function (data) {

                    $localStorage.company_id = data.id;
                    $state.go('main.register_company_additional');

                })
            }

        }

    });
