'use strict';
angular.module('main')
    .controller('ProfileCtrl', function ($localStorage, Restangular, $ionicPopup, Utils, $scope, $log, user) {

        $scope.user = user;
        $scope.credentials = {old_password: '', new_password: '', password_confirmation: ''};

        $scope.update = update;
        $scope.changePassword = changePassword;

        function update() {

            if (!Utils.isAnyFieldEmpty($scope.user.plain())){

                $scope.user.put().then(function (data) {$scope.user = data;})

            }

        }

        function changePassword() {

            if ($scope.credentials.new_password !== $scope.credentials.password_confirmation){

                $ionicPopup.alert({title: "Passwords don't match!"});
                return;

            }

            if (!Utils.isAnyFieldEmpty($scope.credentials)){

                Restangular.one('users', $localStorage.id).all('password').post($scope.credentials).then(function () {

                    $scope.credentials = {old_password: '', new_password: '', password_confirmation: ''};
                    $ionicPopup.alert({title: "Successfully updated!"});

                })

            }

        }

    });
