'use strict';
angular.module('main')
    .directive('chooseVideo', function ($cordovaCamera, $ionicPopup) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {

            },
            scope: {video: "="},
            controller: function ($scope, $element) {

                $element.on('click', function () {

                    navigator.device.capture.captureVideo(captureSuccess, captureError, {duration: 30});

                });

                // Functions

                function captureSuccess(mediaFiles) {

                    console.log("captureSuccess", mediaFiles);

                    mediaFiles[0].getFormatData(function (data) {

                        console.log("data", data);

                        var file = {};

                        file.fullPath = mediaFiles[0].fullPath;
                        file.localURL = mediaFiles[0].localURL;
                        file.duration = data.duration + ' sec';
                        file.size = parseInt(mediaFiles[0].size / 1000000) + ' MB';
                        file.type = mediaFiles[0].type;
                        file.name = mediaFiles[0].name;

                        $scope.video = file;


                    }, function (error) {

                        console.log('A Format Data Error occurred during getFormatData: ' + error.code);

                    });

                }

                function captureError (error) {
                    console.log(error);
                    alert('Returncode: ' + JSON.stringify(error.code));
                }

            }
        };
    });
