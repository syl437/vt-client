'use strict';
angular.module('main')
    .constant('Config', {

        // gulp environment: injects environment vars
        ENV: {
            /*inject-env*/
            'SERVER_URL': 'http://vt.applicazza.net/api/v1'
            /*endinject*/
        },

        // gulp build-vars: injects build vars
        BUILD: {
            /*inject-build*/
            /*endinject*/
        }

    });