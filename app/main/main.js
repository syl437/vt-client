'use strict';
angular.module('main', [
    'ionic',
    'ngCordova',
    'ui.router',
    'restangular',
    'ngStorage',
    'google.places',
    'ngFileUpload'

])
    .config(function ($compileProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider) {

        // Needed to be able to display images in LiveReload mode on devices
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);

        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self'
        ]);

        // ROUTING with ui.router
        $urlRouterProvider.otherwise('/main/login');
        $stateProvider

            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'main/templates/menu.html',
                controller: 'MenuCtrl as menu'
            })

            .state('main.login', {
                url: '/login',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('main.register', {
                url: '/register',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('main.register_company', {
                url: '/register_company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register_company.html',
                        controller: 'RegisterCompanyCtrl'
                    }
                },
                resolve: {
                    company_categories: function (Restangular) { return Restangular.all('company_categories').getList();},
                    regions: function (Restangular) { return Restangular.all('regions').getList();}
                }
            })

            .state('main.register_company_additional', {
                url: '/register_company_additional',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/register_company_additional.html',
                        controller: 'RegisterCompanyAdditionalCtrl'
                    }
                },
                resolve: {company: function (Restangular, $localStorage) {return Restangular.one('companies', $localStorage.company_id).get();}}
            })

            .state('main.home', {
                url: '/home',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/home.html',
                        controller: 'HomeCtrl'
                    }
                },
                resolve: {
                    company_categories: function (Restangular) { return Restangular.all('company_categories').getList();},
                    regions: function (Restangular) { return Restangular.all('regions').getList();}
                }
            })

            .state('main.companies', {
                url: '/companies/:category/:region',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/companies.html',
                        controller: 'CompaniesCtrl'
                    }
                },
                resolve: {companies: function (Restangular, $stateParams) { return Restangular.all('companies').getList({region_id: $stateParams.region, company_category_id: $stateParams.category});}}
            })

            .state('main.profile', {
                url: '/profile',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/profile.html',
                        controller: 'ProfileCtrl'
                    }
                },
                resolve: {user: function (Restangular, $localStorage) { return Restangular.one('users', $localStorage.id).get();}}
            })

            .state('main.login_company', {
                url: '/login_company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/login_company.html',
                        controller: 'LoginCompanyCtrl'
                    }
                }
            })

            .state('main.company', {
                url: '/company',
                views: {
                    'pageContent': {
                        templateUrl: 'main/templates/company.html',
                        controller: 'CompanyCtrl'
                    }
                },
                resolve: {company: function (Restangular, $localStorage) { return Restangular.one('companies', $localStorage.company_id).get();}}

            })
        ;
    })

    .run(function($state, $localStorage, $log, $ionicLoading, Restangular, Config, $ionicPopup){

        // See app/main/constants/env-dev.json and app/main/constants/env-prod.json
        Restangular.setBaseUrl(Config.ENV.SERVER_URL);
        // Restangular.setBaseUrl('http://vt.dev/api/v1');

        // Attach phone number as token for each API request
        Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers) {

            return {
                headers: _.merge({Authorization: 'Bearer ' + $localStorage.token}, headers)
            };

        });

        // handle request

        Restangular.addRequestInterceptor(function(element, operation, what, url){

            $ionicLoading.show({template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'});

            return element;

        });

        // Handle response

        Restangular.addResponseInterceptor(function (data, operation, what, url) {

            if (data.data) {
                $log.log(url, Restangular.copy(data.data).plain());
            }

            var extractedData;

            if (operation === 'getList')
                extractedData = data.data;
            else
                extractedData = data.data;

            $ionicLoading.hide();

            return extractedData;

        });

        Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {

            if (response){

                $ionicLoading.hide();

                switch (response.status){

                    case 401:
                        $ionicPopup.alert({title: "Error 401"});
                        $state.go('main.login');
                        return false;
                        break;

                    case 404:
                        $ionicPopup.alert({title: 'Error 404'});
                        return false;
                        break;

                    case 422:
                        $ionicPopup.alert({title: 'Error 422'});
                        return false;
                        break;

                    default:
                        $ionicPopup.alert({title: 'Error!'});
                        return false;
                        break;

                }

            }

            return true;
        });

        Restangular.addElementTransformer('companies', false, function (file) {

            file.addRestangularMethod('reupload', 'post', undefined, {'_method': 'PATCH'}, {'Content-Type': undefined});

            return file;
        });

    })
;


// сделать валидацию телефона при апдейте профиля по стране
// разобраться с адресом
// exception 401 при несовпадении паролей при изменении пароля - сделать другой
// добавить в исключения gitignore папку media
// сделать отдельный реквест для создания компаний
// переделать логин для компаний на сервере